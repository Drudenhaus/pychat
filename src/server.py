"""
This is the chat server module.
It receives client messages and relays them to all the connected clients.
"""

from __future__ import print_function
import socket
import threading
import sys
import select

# Global variables
__author__ = 'sean'
lock = threading.RLock()
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
address = ('localhost', 8080)
server_socket.bind(address)
server_socket.listen(10)
socket_list = [server_socket]


def relay_message(message):
    """
    Take a message and send it to all the clients.

    :param message: the string repreesenting the message to be relayed to the clients
    :return: None
    """
    # I think that, in theory, this lock will ensure messages are sent in the same order to all clients
    # but not necessarily in the correct order.
    # The API doesn't really describe how the lock is implemented so I'd have to check the internal implementation.
    # Does it do a random spinlock?  If so, then they could be delivered out of order.  Ideally, I'd use a message queue
    # like I did in the client, but time is short and this implementation "works".
    lock.acquire()
    for this_socket in socket_list:
        # Only relay to clients.  The server doesn't need to receive the messages (again).
        if this_socket is not server_socket:
            print('Relaying message:', data.rstrip())
            this_socket.send(message)
        else:
            print('Not relaying to this socket.')
    lock.release()


print('Server is started and listening at', address[0], 'on port', address[1])
try:
    while True:
        # select uses the select() system call (cool!) -- may not be totally compatible with Winders
        read_sockets, write_sockets, error_sockets = select.select(socket_list, [], [])
        for this_read_socket in read_sockets:
            # Create connections for new clients (if the socket isn't in the list then it must be a new connection)
            if this_read_socket is server_socket:
                client_socket, client_addr = server_socket.accept()
                print('Creating socket for incoming connection from:', client_addr[0], 'on port', client_addr[1])
                socket_list.append(client_socket)
            # Otherwise this is an existing client so receive the message and relay it to all the clients
            else:
                try:
                    data = this_read_socket.recv(4096)
                    if data:
                        print('Message received:', data)
                        # Spawn a a thread to send the message
                        thread = threading.Thread(target=relay_message(data))
                        thread.start()
                except socket.error as e:
                    print('ERROR: socket error:', e)
                    print('Closing socket and removing from socket list!')
                    this_read_socket.close()
                    socket_list.remove(this_read_socket)
# ctrl+c will be the normal way to exit so handle this exception
# Alternatively, I could use the signal (SIGINT)
except KeyboardInterrupt as ke:
    print('Caught keyboard interrupt!  Closing sockets!')
    for this_read_socket in socket_list:
        this_read_socket.close()
    sys.exit(0)