"""
This is the chat client module.
It will create a Tkinter-based client from which the user can send/receive messages to/from the server.
"""

from __future__ import print_function
import ScrolledText
import Tkinter as Tk
import tkMessageBox
import socket
import sys
import pickle
import threading
import collections

# Global variables
__author__ = 'sean'
# Important note: Python's deque is atomic while its queue is not!
message_deque = collections.deque()
address = ('localhost', 8080)


def menu_open():
    """
    Open a chat log and insert the log into the ScrolledText widget.

    :return: None
    """
    try:
        with open('log.bin', 'rb') as log_file:
            contents = pickle.load(log_file)
            messages_text.configure(state='normal')
            messages_text.delete('1.0', Tk.END+'-1c')
            messages_text.insert(Tk.END, contents)
            messages_text.configure(state='disabled')
            tkMessageBox.showinfo('Loaded', 'Log loaded.')
    except pickle.UnpicklingError as upe:
        error_str = 'ERROR: could not pickle data: ', upe
        tkMessageBox.showerror('Error!', error_str)


def menu_save():
    """
    Save the current chat log to a binary file.

    :return: None
    """
    messages_text.configure(state='normal')
    contents = messages_text.get('1.0', Tk.END+'-1c')
    try:
        with open('log.bin', 'wb') as log_file:
            pickle.dump(contents, log_file)
        tkMessageBox.showinfo('Saved', 'Log saved.')
    except pickle.PickleError as pe:
        error_str = 'ERROR: could not pickle data: ', pe
        tkMessageBox.showerror('Error!', error_str)
    messages_text.configure(state='disabled')


def menu_exit():
    """
    Close the sockets and then exit the program.

    :return: None
    """
    send_socket.close()
    receive_socket.close()
    sys.exit(0)


def receive_messages():
    """
    Receive messages inside a loop.
    This function is run in a thread.

    :return: None
    """
    global message_deque
    while True:
        try:
            while True:
                response = receive_socket.recv(4096)
                if response:
                    message_deque.append(response)
        except socket.error as se:
            print('ERROR: connection error:', se)
            pass


def send_message():
    """
    Get the username and message and send it to the server.

    :return: None
    """
    if not username_input.get():
        tkMessageBox.showerror('Error!', 'Username field is empty.')
    else:
        message = (username_input.get() + ':\t' + message_input.get() + '\n')
        try:
            send_socket.send(message)
        except socket.error as ce:
            print('ERROR: connection error:', ce)
            sys.exit(-1)


def monitor_queue():
    """
    Check the message queue (deque) for new messages.
    Pop and add them to the ScrolledText widget.
    This function runs every 200 ms.

    :return:
    """
    global message_deque
    if len(message_deque) > 0:
        messages_text.configure(state='normal')
        new_str = message_deque.popleft()
        messages_text.insert(Tk.END, new_str)
        messages_text.yview(Tk.END)
        messages_text.configure(state='disabled')
    main.after(200, monitor_queue)


# Setup the sockets
try:
    send_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    receive_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except socket.error, e:
    print('ERROR: failed to get socket:', e)
    sys.exit(-1)
try:
    send_socket.settimeout(10)
    send_socket.connect(address)
    receive_socket.settimeout(10)
    receive_socket.connect(address)
    print('Connected to server at', address[0], 'on port', address[1])
except socket.gaierror as gaie:
    print('ERROR: could not connect to server address:', gaie)
    sys.exit(-1)
except socket.error as error:
    print('ERROR: connection error:', error)
    sys.exit(-1)


# Setup the Tkinter GUI
main = Tk.Tk()
main.title('Client')
main.geometry('580x400')
main.resizable(False, False)

menu_bar = Tk.Menu(main)
file_menu = Tk.Menu(menu_bar, tearoff=0)
file_menu.add_command(label='Open Log', command=menu_open)
file_menu.add_command(label='Save Log', command=menu_save)
file_menu.add_command(label='Exit', command=menu_exit)
menu_bar.add_cascade(label='File', menu=file_menu)

main.config(menu=menu_bar)

messages_text = ScrolledText.ScrolledText(master=main, wrap='word', state='disabled', width=80, height=25)
messages_text.grid(row=0, column=1, columnspan=3)
username_label = Tk.Label(main, text='Username')
username_label.grid(row=1, column=1)
username_input = Tk.Entry(main)
username_input.grid(row=1, column=2)

message_label = Tk.Label(main, text='Message')
message_label.grid(row=2, column=1)
message_input = Tk.Entry(main)
message_input.grid(row=2, column=2)
message_input.focus()
message_input.bind('<Return>', send_message)
message_button = Tk.Button(main, text='Send', command=send_message)
message_button.grid(row=2, column=3)

# Spawn the receive thread
receive_thread = threading.Thread(target=receive_messages)
# setting daemon = true will cause the thread to exit upon program termination
receive_thread.daemon = True
receive_thread.start()

# Start monitoring the message queue
main.after(200, monitor_queue)

main.mainloop()